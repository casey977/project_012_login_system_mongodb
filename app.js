'use strict';

const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bp = require('body-parser');
const fs = require('fs');

let setup = fs.readFileSync('../setup.json');
setup = JSON.parse(setup);

const PORT = setup.port;
const DB_URL = setup.db_url;
const DB_NAME = setup.db_name;
const DB_COLL_USERS = setup.db_coll_users;

let current_users = [];

try {
	const client = new MongoClient(DB_URL);
	const db = client.db(DB_NAME);
	const collection = db.collection(DB_COLL_USERS);
} catch (error) {
	console.error('ERROR!', error);
}

let app = express();
app.use(bp.json());

app.get('/', func_main);
app.post('/', func_login);

app.listen(PORT, func_listen);

function func_main(req, res){
	try {
		res.sendFile(__dirname + '/public/index.html');
	} catch (error) {
		console.error('ERROR!', error);
	}
	
}

function func_login(req, res){
	try {
		const data = req.body;
		const le_username = data.usr;
		const le_password = data.pwd;
		let info;

		client.connect(func_db_connect);

		collection.findOne({username: le_username, password: le_password}).toArray((error, documents) => {
			if (error) {
				console.error('ERROR: ', error);
				return;
			} else if(documents.length === 0) {
				console.log('Username and/or password not found.');
			} else {
				console.log('Username and password found!', documents);
			}
		});

		client.close();

		//return res.json({'token': jsonwebtoken.sign({'user': HARDCODED_USERNAME}, JWT_SECRET)});

	} catch (error) {
		console.error('ERROR: ', error);
		res.status(401).send('Login failed.');
	}
}

function func_db_connect(error){
	if(error){
		console.error('Failed to connect to the database:', err);
	} else {
		console.log('Connected to the database successfully.');
	}
}

function func_listen(){
	console.log(`Server running with port ${PORT}`);
}